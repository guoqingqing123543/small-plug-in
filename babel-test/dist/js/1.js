import { module } from "browserify/lib/builtins";

class Phone {
    constructor(price, color, name, inch) {
        this.price = price;
        this.color = color;
        this.name = name;
        this.inch = inch;
    }

    static call(num) {
        console.log('call!', num);
    }
}

module.exports = {
    XM: new Phone(299, 'red', '小米', '199X205'),
    b: 2,
    speak(val) {
        console.log(val);
    }
};