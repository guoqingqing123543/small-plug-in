let a = 'a'
let b = 1
let c = []
let d = {}
let e = function () {
}
let f = null
let g = undefined
let h = NaN
let i = true
let j = Symbol('aolifgie')
let k = BigInt(100)
console.log(Object.prototype.toString.call(a) == '[object String]');
console.log(Object.prototype.toString.call(b) == '[object Number]');
console.log(Object.prototype.toString.call(c) == '[object Array]');
console.log(Object.prototype.toString.call(d) == '[object Object]');
console.log(Object.prototype.toString.call(e) == '[object Function]');
console.log(Object.prototype.toString.call(f) == '[object Null]');
console.log(Object.prototype.toString.call(g) == '[object Undefined]');
console.log(Object.prototype.toString.call(h) == '[object Number]', 'NaN 的数据类型属于number');
console.log(Object.prototype.toString.call(i) == '[object Boolean]');
console.log(Object.prototype.toString.call(j) == '[object Symbol]');
console.log(Object.prototype.toString.call(k) == '[object BigInt]');
