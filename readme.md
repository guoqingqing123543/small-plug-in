# 关于移动端学习记录的知识点

h5新增的标签有 header nav aside selection article footer

### 分列布局

针对一个块级元素的内容

想要实现块级元素水平居中 要保证自己有宽度 还要保证父级是块级元素

`  column-count: 2;` 实现自动分列 数字代表列数
`column-gap: 30px;` 列间距 超出容器会默认溢出
``

# 模仿世纪佳缘官网 为了证明自己的能力

### 版心宽度 1000px 分为七个栏目

        主要记录开发遇到的问题
        (按两次回车就取消了自动递增);
        md文档语法也需要学习，打字也需要学习

1. 栏目一 logo 以及登录框 高 70px
2. 栏目二 注册表单 高 450px
3. 栏目三 小长条 宣传标语 高 78px
4. 栏目四 优质会员展示 以及会员条件筛选 高 727(本身没有定义高度 由内容撑开的) padding 30 0
5. 栏目五 配对成功展示 高 404(本身没有定义高度 由内容撑开的) padding 30 0
6. 栏目六 申请入口 高 115px 以上三个都是相对定位
7. 栏目七 footer 高96(本身没有定义高度 由内容撑开的) padding-top 20
8. 栏目八 悬浮 引导快捷登录 高 60px

### 1. 栏目一 logo 以及登录框 高 70px 开发记录

头部logo注重seo 所以使用 h1 a img img固定宽和高，使用左右常规布局

        clearfix的具体用法
        placeholder的样式设置 -webkit-input-placeholder
        图片与文字布局 在 a和img之间的协调问题
        a标签的下划线 text-decoration: none;

bigRegister

    伪元素若选择 absolute 则默认相对于它最近的一个relative 若无 相对于body

    类似padding margin 的值 四个值就是 上 右 下 左 三个值就是上 左右 下 两个值上下 左右 一个值 上下左右

    对于行内文字想要超出不换行需要使用 overflow:hidden 还有 text-overflow：ellipsis

# jquery笔记

    导航栏打开影响到了下方的div
    
    使自己的position 变为absolute 默认static

    stop 写在动画的前边代表只执行最新的一次 类似节流函数

    fadeIn fadeOut 淡入淡出

    slideUp slideDown 滑出滑入

    animate 移动

    使容器水平垂直都居中需要 position:absolute 上下左右零 margin ：auto

    trigger 可以触发自定义事件
    
    $.fn是对象 新增在此对象中的函数使用需要 $('selector') 

# nodejs 的事件轮询

	基于libuv库（GitHub可查询源码）
	查找 core文件中的 uv_run 函数 即实现原理

# rem适配

	让某一元素在不同机型上呈现相同的比例
	html的字体大小 html{font-size:36px} rem就是相对于html的font-size 的比例
	----------------
	另一种写法
	设定主流设备的宽度
	假定能接受的最大字体为40px
	当前设备宽度/主流设备宽度 得到一个接近 100%的比例
	文档要求的字体大小 例如 20px 乘以这个比例 就实现了 动态调整rem 
	这种计算方法 就代表 screen width / goodSize 就是100%宽度
	一般移动手机宽度 480px
	平板特殊
	那么除以相应的比例就是容器占宽度的比例

表单书写相关知识

    单选 input type= radio id = 属性名 name 代表参数的名称（属性名） value = 属性值  （label for 指向id ）
    多选 input type= checkbox id = 属性名 value = 属性值  （label for 指向id ）
    下拉框 select name = 属性名 包裹的option > value = 属性值 

##### 属于form中的标签那key值就是 name属性 value就是value值

# 轮播图

    一个指定大小的容器
    里边嵌套一个很长的容器 要让这个容器溢出隐藏
    使用position absolute 控制left值

# querySelector 只获取第一个元素 ，获取到的对象可以直接操作dom

# 外边距合并的问题获取可通过display inline-block 解决

# 名词

	opacity 不透明度 opacity 
	setAttribute 原生js 添加属性（dom的api）
	不透明度 opacity 透明度rgba值 rgba(X,X,X,0.2) 最后一个值
	定位的单词拼写 position , relative, absolute,static
	布局方式的单词 display, flex,block,inline-block,table-cell
	disabled 禁用
    transition 过渡
    transform 动画
    translate 位移

# 画三角形

    border 是复合属性
    border-top border-left border-bottom border-right
    值的格式 尺寸（一般px） 类型（一般solid） 颜色（颜色值 或transparent）
    
    例如 朝上的三角形
    *{
        width:0
        height:0
        border-top ： 10px solid transparent
        border-left : 10px solid transparent
        border-right: 10px solid transparent
        border-bottom ： 10px solid #f40
        // 这样就实现了一个等腰三角形 底边长20px 高10px （但占的空间是 20px * 20px，做定位时需要特别注意）
        //宽度和高度也是需要写的 因为要排除继承的可能 
        //主要是利用盒模型的border内容区的边界处是一条斜线
        // 拓展 还可以 实现 斜角 类似 朝右下角的三角形
        width:0
        height:0
        border-top ： 10px solid transparent
        border-left : 10px solid transparent
        border-right: 10px solid  #f40
        border-bottom ： 10px solid #f40
    }

# ~ css选择器

    可以命中当前选择器的兄弟选择器

    例如 .box:hover ~ p{
        width:80px
    } 
    class为box悬停 时 p元素就会应用下边的css规则

# 清除浮动 after

    *:after,clear{
        content:'';
        display:block;
        clear:both;
    }
