let speed = 100
$(document).ready(function () {
    //导航栏 刚加载使导航栏子菜单隐藏
    $('.nav > li').css({
        float: 'left',
        width: '100px',
        padding: '20px 11.5px',
        textAlign: 'center',
        backgroundColor: 'whitesmoke'
    }).parent('.nav').css({
        width: '500px',
        margin: '0 auto',
    })

    $("a").css({
        textDecoration: 'none',
        color: 'black'
    })
    $('.nav').children().children('ul').slideUp(speed)
})

$(document).ready(function () {

    $('.nav > li').hover(function () {
        $(this).children('ul').stop().slideToggle()
    })
})

$(document).ready(function () {
    $('.item2 li').eq(0).css({
        color: 'yellow'
    })
    $('li[class^="sub-item"]').click(function (e) {

        console.log($(this).parents('li[class^="item"]').children('a').text())
    })
})
//购物车全选交互优化
$(document).ready(function () {
    let all_input = $('input[name="allSelect"]')
    //单选修改自己的checked状态
    $('.table input[name^="goods"]').change(function () {
        //item被选中后更新自己的属性状态 prop 代表获取设置原生属性 attr自定义数据
        let a = $('input[name^="goods"]:checked').length
        let b = $('input[name^="goods"]').length
        a === b ? all_input.prop('checked', true) : all_input.prop('checked', false)
    })
    //全选后 修改所有item的状态
    $('.table input[name="allSelect"]').change(function () {
        let all_checked = $(this).prop('checked')
        $('.table input[name^="goods"],input[name="allSelect"]').prop('checked', all_checked)
        $(this).prop('checked', all_checked)
        console.log($(this).prop('checked'))
    })

    //item项的小计动态化

    function ctl_count(val) {
        let price = $(this).parent().siblings('.unit_price').text().trim();
        let count = (price * val).toFixed(2)
        $(this).parent().siblings('.count').text(count)


    }

    function total() {
        //实现合计
        let countsDom = $('td.count');
        let total = 0;
        $(countsDom).each(function (index, dom) {

            let item_price = parseFloat($(dom).text().trim())
            total += item_price
        })
        $('.total-price span').text(total.toFixed(2))
    }

    total()

    $('.increment').click(function () {
        let val = $(this).siblings('.pcs').text() * 1 + 1
        $(this).siblings('.pcs').text(val)

        ctl_count.call(this, val)
        total()
        // let price = $(this).parent().siblings('.unit_price').text().trim();
        // let count = (price * val).toFixed(2)
        // $(this).parent().siblings('.count').text(count)
    })
    $('.decrement').click(function () {
        let val = $(this).siblings('.pcs').text()
        if (val * 1 < 1) {
            return alert('val：' + val + '. 不能再少了! ')
        }
        let new_val = val * 1 - 1
        $(this).siblings('.pcs').text(new_val)
        ctl_count.call(this, new_val)
        total()
    })
    //完善删除选中数据的操作
    $('.del_checked').click(function () {
        //找到被选中的dom的父级tr
        //移除
        $('[name^="goods"]:checked').parents('tr').remove()
        //更新商品序号
        $('td.sn label').each(function (index, dom) {
            $(dom).text(index + 1 + '')
        })
        // 商品列表若为零就将全选按钮的对号置空并禁用
        $('[name^="goods"]').length === 0 ? ($('[name="allSelect"]').prop({
            checked: false,
            disabled: true
        }), $(this).hide()) : void 0
    })
})



