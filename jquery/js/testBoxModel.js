function testBoxModel() {
    let target = $('.wrap')
    let boxModel = {
        宽度: target.width(),
        宽度加内边距: target.innerWidth(),
        宽度加内边距加边框: target.outerWidth(),
        宽度加内边距加边框加外边距: target.outerWidth(true),
        offset: target.offset(),
        position: target.position()
    }
    // offset 相对于文档 可修改值 ； position 相对于最近的 relative 不可修改值； 注意dom隐式的默认偏移 8px 6px

    let offset = $('.box').offset({
        left: 800,
        top: 1000
    })
    $.each(boxModel, function (key, val) {
        console.log(key, val)
    })
}

function handleFloor() {
    let screenH = $(window).height()

    let native = screen.availHeight
    $('.nav li').click(function () {
        let index = $(this).index()
        let floor = $('.floor_parent div')
        let floorDom = $(floor[index])
        let targetTop = floorDom.offset().top - floor.height() * 0.5
        $('body,html').animate({
            left: 0,
            scrollTop: targetTop
        }, 300)
        console.log(floorDom)
    })
}

$(document).ready(function () {
    testBoxModel()
    $('.back-top').click(function () {
        console.log(this)
        $('body,html').animate({
            scrollTop: 0,
        }, 200)
    })
    $(window).scroll(function () {
        let target = $('.wrap')
        if ($(this).scrollTop() > target.offset().top) {
            $('.back-top').stop().fadeIn()
        } else {
            $('.back-top').stop().fadeOut()
        }

    })

    handleFloor()

})