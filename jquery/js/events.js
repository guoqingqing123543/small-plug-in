$(document).ready(function () {
    $('.box').on({
        'click': function (e) {
            console.log('点击了', this)
            $(this).remove()
        },
        mouseenter: function (e) {
            console.log('鼠标移入')
            $(this).css({
                fontSize: '28px',
                color: 'red'
            })
        },
        mouseleave: function () {
            console.log('鼠标离开')
            $($(this).css({
                fontSize: '27px',
                color: 'yellow'
            }))
        }
    })
    $('.box1').on('click', 'p', {name: 'gqq'}, function (event) {
        console.log(event)
    })
    let sendBtn = $('input[type="button"]')
    sendBtn.on('click', function (event) {
        let message = $('textarea').val()
        $(this).siblings('ul').append(`<li><span>${message} </span><a href="#">删除</a></li>`)
        $('textarea').val('')
        if ($(this).siblings('ul').children('li').length > 0) {
            $(this).siblings('p').hide()
        }
        sendBtn.off('click')
    })
    //绑定事件 on 事件监听 可写对象 key 事件名 value 事件处理函数 可写args 1 事件名 2 委托目标 3 传递参数 4 事件处理函数（event中有 data：3） 与 事件解绑 off 与  单次执行后销毁 one

    sendBtn.one({
        click: function () {
            console.log(this)
        }
    })

    let historyBox = $('ul');
    historyBox.on('click', 'a', function (e) {
        console.log($(this).parent().remove());
    })

})