$(document).ready(function () {
    function noneLine() {
        let dom = $('body').children()
        dom.each(function (index, dom) {
            setTimeout(function () {
                $(dom).fadeOut()
            }, 1500 * index * 2)
        })
        console.log(dom)
    }

    $('h1').on('click', function () {
        $(this).css({
            color: 'red'
        })
    })
    $('textarea').on('focus', function (e) {
        console.log(e)
        $(this).val(new Date() + '卧槽 全自动化')
    })

    /**
     * trigger 触发绑定过的事件
     *
     * triggerHandler 触发绑定事件 但不会有默认行为 例如 光标闪烁
     * */
    // setTimeout(function () {
    //     $('textarea').trigger('focus')
    // }, 3000)
    //
    setTimeout(function () {
        $('h1').triggerHandler('click')
        $('textarea').trigger('focus')
    }, 2000)
})

