$(document).ready(function () {
    let target = $('.animate-box')
    $('.animate-btn').click(function (e) {
        e.preventDefault()
        console.log($(target).hasClass('moved'))
        if ($(target).hasClass('moved')) {


            return target.attr('style', '').removeClass('moved')
        }
        $(target).animate({
            width: "90px",
            height: "100px",
            fontSize: "10em",
            borderWidth: 10,
            borderRadius: '20px',
            left: '300px'
        }).addClass('moved')

    })
});